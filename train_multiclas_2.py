import pandas as pd
import numpy as np
import gc
import pyspark
from pyspark.sql import SparkSession
from config import *
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
import lightgbm as lgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.preprocessing import LabelEncoder
import pickle
from hdfs import InsecureClient
import sys


np.random.seed(78)
spark = SparkSession.builder.appName("RS_train_multiclass").getOrCreate()
spark.sparkContext.setLogLevel("ERROR")


class AssertGoodHeader(BaseEstimator, TransformerMixin):
    """
    Doan nay khoi  tao
    """
    def __init__(self):
        self.columns = None

    def fit(self, x, y=None):
        self.columns = x.columns
        return self

    def transform(self, x, y=None):
        return x[self.columns]


class DropCorrFeats(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.list_of_drop_feats = []

    def fit(self, df, y=None):
        df_corr = df.corr()  # list_of_drop_feats = []
        for i in range(df_corr.shape[0]):
            for j in range(i + 1, df_corr.shape[0], 1):
                if df_corr.iloc[i, j] >= 0.9:
                    self.list_of_drop_feats.append(df_corr.index[i])
        return self

    def transform(self, df, y=None):
        if len(self.list_of_drop_feats) > 0:
            df = df.drop(self.list_of_drop_feats, axis=1)
        return df


class DropNullFeats(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.list_of_drop_feats = []

    def fit(self, df, y=None):
        self.list_of_drop_feats = []
        df_null = df.isnull().sum().reset_index()
        df_null.columns = ['Features', 'NBR_NULL']
        df_null['Percent_Null'] = df_null['NBR_NULL'] / df.shape[0]
        self.list_of_drop_feats = df_null[df_null['Percent_Null'] >= 0.999]['Features'].tolist()
        return self

    def transform(self, df, y=None):
        if len(self.list_of_drop_feats) > 0:
            df = df.drop(self.list_of_drop_feats, axis=1)
        return df


class FillNa(BaseEstimator, TransformerMixin):
    def __init__(self, fill_method='mean'):
        self.fillvalue = None
        self.fillvalue = []
        self.fill_method = fill_method

    def fit(self, df, y=None):
        self.num_feats = [col for col in df.columns if df[col].dtypes != 'object' and 'recency' not in col]
        self.special_feats = [col for col in df.columns if 'recency' in col]
        if self.fill_method == 'mean':
            self.fillvalue = df[self.num_feats].mean()
        if self.fill_method == 'median':
            self.fillvalue = df[self.num_feats].median()
        if self.fill_method == 'zero':
            self.fillvalue = 0
        return self

    def transform(self, df, y=None):
        df[self.num_feats] = df[self.num_feats].fillna(self.fillvalue)
        for col in self.special_feats:
            df[col] = df[col].fillna(99)
            df[col] = df[col].astype('int')
        return df


class LabelEncoderAll(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.dict_ = {}
        self.cat_cols = []
        self.unknownkey = 'unknown'

    def fit(self, x, y=None):
        self.cat_cols = [col for col in x.columns if x[col].dtypes == 'object']
        if len(self.cat_cols) > 0:
            print('start to label encoder...')
        for col in self.cat_cols:
            print(col, end=' ')
            index = x[col].value_counts().index
            self.dict_[col] = dict(zip(index, range(len(index))))
            self.dict_[col][self.unknown_key] = -1
        return self

    def transform(self, x, y=None):
        for col in self.cat_cols:
            x[col] = x[col].map(lambda x_: x_ if x_ in self.dict_[col].keys() else self.unknown_key)
            x[col] = x[col].map(self.dict_[col])
        return x


def lgb_multiclass(k):
    model = lgb.LGBMClassifier(
        objective='muticlass',
        num_class=k,
        max_depth=50, random_state=49,
        metric='multi_logloss',
        num_leaves=45,
        learning_rate=0.03,
        boosting_type='gbdt',
        is_unbalance=True)


if __name__ == "__main__":

    # DOC DU LE VA CHUYEN SANG PANDAS
    df_feat = spark.read.parquet(MULTICLASS_FEAT_WRITE_DIR + '/%s' %VERSION + '/feature_icons')
    df_label = spark.read.parquet(WRITE_LABEL_MATRIX_DIR)
    df_full = df_feat.join(df_label, 'msisdn', 'inner').persist(pyspark.StorageLevel.DISK_ONLY_2)
    print(f"so dong cua data dua vao training: {df_full.count()}")
    selected_columns = [col for col in df_full.columns if (('frequency' in col) | ('recency' in col))]
    # chi lay feat recency, frequency, bo trans_amount
    df_clean = df_full.\
        select(selected_columns + [col for col in df_full.columns if 'true_' in col] + ['msisdn']).\
        persist(pyspark.StorageLevel.DISK_ONLY_2)
    df_clean.count()
    print("cat ra convert sang pandas de tranh het RAM, neu chet o day thi fix bang cach ach nhieu in hon")
    (df_clean1, df_clean2) = df_clean.randomSplit([0.5, 0.5])
    df_clean1 = df_clean1.toPandas()
    df_clean2 = df_clean2.toPandas()
    print(f"tong: {df_clean1.shape[0] + df_clean2.shape[0]}")
    dfs = [df_clean1, df_clean2]
    df_input = pd.concat(dfs, axis=0)
    # XU LY TIEN DU LIEU
    for i in range(0, 6):
        # fill na and delete row
        df_input['true_'+str(i+1)] = df_input['true_'+str(i+1)].fillna('None')
        data = df_input[df_input['true_'+str(i+1)] != 'None'][selected_columns]
        # encoding 1,2,3,... for label
        le = LabelEncoder().fit(df_input[df_input['true_'+str(i+1)] != 'None']['true_'+str(i+1)])
        target = le.transform(df_input[df_input['true_'+str(i+1)] != 'None']['true_'+str(i+1)])
        # train tet split
        X_train, X_test, y_train, y_test = train_test_split(data, target, random_state=49, test_size=0.3)
        # number of  class
        k_output = df_input[df_input['true_'+str(i+1)] != 'None']['true_'+str(i+1)].nunique()
        print(k_output)
        model_lgb = lgb.LGBMClassifier(objective='multiclass', metric='multi_logloss',
                                       num_leaves=45, learning_rate=0.03,
                                       boosting_type='gbdt', max_depth=50,
                                       is_unbalance=True, num_class=k_output)
        pipeline = Pipeline(steps=[('assert', AssertGoodHeader()), ('Drop_Corr_Feat', DropCorrFeats()),
                                   ('Drop_Null_Fea', DropNullFeats()),
                                   ('FillNull', FillNa(fill_method='zero')),
                                   ('Label_Encode', LabelEncoderAll()),
                                   ('mode', model_lgb)])
        pipeline.fit(X_train, y_train)
        predict = pipeline.predict(X_test)
        precision = precision_score(predict, y_test, average=None).mean()
        print(precision)

    # saving encoder and model
    #encoder_file = MULTICLASS_ENCODER_DIR + str(i+1) + "_" + VERSION+'.pkl'
    #with open(encoder_file, 'wb') as file:
        #pickle.dump(le, file)

    # save pipeline
    #model_file = MULTICLASS_MODEL_DIR + str(i+1) + "_" + VERSION+'.pkl'
    #with open(model_file, 'wb') as file:
        #pickle.dump(pipeline, file)

    #col_file = MULTICLASS_COLTRAIN_DIR + "_" + VERSION + ".pkl"
    #with open(col_file, 'wb') as file:
        #pickle.dump(selected_columns, file) 
    
    client = InsecureClient('http://54.208.13.102:9870', user='hdoop')
    for i in range(0, 6):
        encoder_file = MULTICLASS_ENCODER_DIR + str(i+1) + "_" + VERSION+'.pkl'
        with client.write(encoder_file, overwrite=True) as f:
            pickle.dump(le, f)

        

        model_file = MULTICLASS_MODEL_DIR + str(i+1) + "_" + VERSION+'.pkl'
        with client.write(model_file, overwrite=True) as f:
            pickle.dump(pipeline, f)

        col_file = MULTICLASS_COLTRAIN_DIR + "_" + VERSION + ".pkl"
        with client.write(col_file, overwrite=True) as f:
            pickle.dump(selected_columns, f)
         
    # XOA BO NHO TREN LOCAL
    del df_input
    gc.collect()

spark.stop()
