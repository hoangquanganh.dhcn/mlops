import os
from pathlib import Path

FINANCIAL_TRANSACTION_DIR = "/opt/spark/examples/FINANCIAL_TRANSACTION_DIR .parquet"
START_DATE_3MONTHS = "20220801"
END_DATE = "20221031"
REQUEST_DATE_FINAL  = "2022-10-31"
LIST_MONTH = [8,9,10]
MAP_ICON_LEVEL1_DIR = "/opt/spark/examples/level1.csv"
MAP_ICON_LEVEL0_DIR = "/opt/spark/examples/level0.csv"
ETC_PAY_TRANS = "/opt/spark/examples/ETC_PAY_TRANS .parquet"
TRANS_GW_DIR = "/opt/spark/examples/TRANS_GW_DIR.parquet"
MULTICLASS_FEAT_WRITE_DIR = 'hdfs://54.208.13.102:9000/feature'
VERSION = "202212"
VERSION_ICON_MAP = '202210'
WRITE_LABEL_TRANS_DIR = "/opt/spark/examples/WRITE_LABEL_TRANS_DIR .parquet"
WRITE_LABEL_MATRIX_DIR = "/opt/spark/examples/WRITE_LABEL_MATRIX_DIR .parquet"
MULTICLASS_ENCODER_DIR = "/save_model/label_encoder_top"
MULTICLASS_MODEL_DIR = "/save_model/Pipeline_Model_top"
MULTICLASS_COLTRAIN_DIR = "/save_model/col_train" 
MULTI_ICONS = 6
NO_ICONS_PREDICT = 24
DATE_UPDATE = "20221101"
ALL_SERVICE_CODE = "/opt/spark/examples/ALL_SERVICE_CODE.parquet"
MULTICLASS_FINAL_RES_DIR = "hdfs://54.208.13.102:9000/predict"
