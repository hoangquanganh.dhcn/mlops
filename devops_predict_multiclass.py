from pyspark.sql.functions import *
from pyspark.sql.types import ArrayType, StringType
from pyspark.sql.session import SparkSession
import pyspark
from pyspark.sql.functions import col
import pandas as pd
import pickle
from config import *
from sklearn.base import BaseEstimator, TransformerMixin
from train_multiclas_2 import *
from hdfs import InsecureClient
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
import sklearn 
spark = SparkSession.builder.appName("RS_predict_multiclass").getOrCreate()
spark.sparkContext.setLogLevel("error")

client = InsecureClient('http://54.208.13.102:9870', user='hdoop')

class AssertGoodHeader(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.columns = None

    def fit(self, x, y=None):
        self.columns = x.columns
        return self

    def transform(self, x, y=None):
        return x[self.columns]


class DropCorrFeats(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.list_of_drop_feats = []

    def fit(self, df, y=None):
        df_corr = df.corr() # list_of_drop_feats = []
        for i in range(df_corr.shape[0]):
            for j in range(i+1, df_corr.shape[0], 1):
                if df_corr.iloc[i, j] >= 0.9:
                    self.list_of_drop_feats.append(df_corr.index[i])
        return self

    def transform(self, df, y=None):
        if len(self.list_of_drop_feats) > 0:
            df = df.drop(self.list_of_drop_feats, axis=1)
        return df


class FillNa(BaseEstimator, TransformerMixin):
    def __init__(self, fill_method='mean'):
        self.fillvalue = None
        self.fillvalue = []
        self.fill_method = fill_method

    def fit(self, df, y=None):
        self.num_feats = [col for col in df.columns
                          if df[col].dtypes != 'object' and 'recency' not in col]
        self.special_feats = [col for col in df.columns if 'recency' in col]
        if self.fill_method == 'mean':
            self.fillvalue = df[self.num_feats].mean()
        if self.fill_method == 'median':
            self.fillvalue = df[self.num_feats].median()
        if self.fill_method == 'zero':
            self.fillvalue = 0
        return self

    def transform(self, df, y=None):
        df[self.num_feats] = df[self.num_feats].fillna(self.fillvalue)
        for col in self.special_feats:
            df[col] = df[col].fillna(99)
            df[col] = df[col].astype('int')
        return df


class LabelEncoderAll(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.dict_ = {}
        self.cat_cols = []
        self.unknown_key = 'unknown'

    def fit(self, x, y=None):
        self.cat_cols = [col for col in x.columns if x[col].dtypes == 'object']
        if len(self.cat_cols) > 0:
            print('start to label encoder...')
        for col in self.cat_cols:
            print(col, end=' ')
            index = x[col].value_counts().index
            self.dict_[col] = dict(zip(index, range(len(index))))
            self.dict_[col][self.unknown_key] = -1
        return self

    def transform(self, x, y=None):
        for col in self.cat_cols:
            x[col] = x[col].map(lambda x_input: x_input if x_input in self.dict_[col].keys() else self.unknown_key)
            x[col] = x[col].map(self.dict_[col])
        return x


def multiclass_predict(df_backtest):
    print("cat ra convert sang pandas de tranh het RAM, neu chet o day thi fix bang cach tach nhieu bien hon")
    (df_backtest1, df_backtest2, df_backtest3) = df_backtest.randomSplit([0.3, 0.3, 0.4])
    df_backtest1 = df_backtest1.toPandas()
    df_backtest2 = df_backtest2.toPandas()
    df_backtest3 = df_backtest3.toPandas()
    print(f"so dong cua data dua vao training: {df_backtest.count()}")
    print(f"neu ket qua bang so dong data dua vao training thi split khong bi thieu du lieu: "
          f"{df_backtest1.shape[0]+df_backtest2.shape[0]+df_backtest3.shape[0]}")

    for i in range(0, MULTI_ICONS):
        print(i)
        print("predict1")
        predict1 = loaded_model.predict(df_backtest1.drop([col for col in
                                                          df_backtest1.columns if 'predict' in col], axis=1))
        df_backtest1['predict_'+str(i+1)] = le.inverse_transform(predict1)
        print("predict2")
        predict2 = loaded_model.predict(df_backtest2.drop([col for col in
                                                           df_backtest2.columns if 'predict' in col], axis=1))
        df_backtest2['predict_'+str(i+1)] = le.inverse_transform(predict2)
        print("predict3")
        predict3 = loaded_model.predict(df_backtest3.drop([col for col in
                                                           df_backtest3.columns if 'predict' in col], axis=1))
        df_backtest3['predict_'+str(i+1)] = le.inverse_transform(predict3)
    df_predict_full = pd.concat([df_backtest1, df_backtest2, df_backtest3], axis=0)
    return df_predict_full


def find_element(x, y, i):
    y_minus_x = [item for item in y if item not in x]
    return y_minus_x[:i]


def mix_hot_services(label_trans, df_final):
    df = label_trans.select("*").\
        withColumnRenamed("msisdn", "userId").\
        withColumnRenamed("service_norm", "movieId").toPandas()

    hot_service = df['movieId'].value_counts()[:NO_ICONS_PREDICT].index

    df_final['predict_service'] = df_final[[col for col in df_final.columns if (('predict' in col) and ('service' not in col))]].\
        apply(lambda x: list(dict.fromkeys(x.tolist())), axis=1)
    df_final['nbr_icon'] = df_final['predict_service'].apply(lambda x: len(x))
    df_final['predict_service_full'] = df_final[['predict_service', 'nbr_icon']].\
        apply(lambda x: find_element(x['predict_service'], hot_service, NO_ICONS_PREDICT -
                                     x['nbr_icon'])[:(NO_ICONS_PREDICT-x['nbr_icon'])], axis=1)
    df_final['predict_service_full'] = df_final['predict_service'] + df_final['predict_service_full']
    for i in range(NO_ICONS_PREDICT):
        df_final['predict_'+str(i+1)] = df_final['predict_service_full'].\
            apply(lambda x: x[i])
    # chi giu lai cot predict va sdt
    df_final = df_final.drop(['nbr_icon', 'predict_service', 'predict_service_full'], axis=1).\
        drop([col for col in df_final.columns if (('frequency' in col) | ('recency' in col))], axis=1)
    return df_final


def predict_upv_(res):
    matrix = spark.read.parquet(WRITE_LABEL_MATRIX_DIR)
    actual_upv = matrix.select('msisdn',
                               expr("""stack(11, "1", true_1, "2", true_2, "3", true_3, "4", true_4, 
                               "5", true_5,"6", true_6, "7", true_7, "8", true_8, "9", true_9, "10",
                               true_10, "11", true_11) as (rank, value)""")).\
        where('value is not null')
    predict_upv = res.select('msisdn',
                             expr("""stack(24, '1', predict_1, '2', predict_2, '3', predict_3, '4', predict_4, 
                            '5', predict_5, '6', predict_6, '7', predict_7, '8', predict_8, '9', predict_9, 
                            '10', predict_10, '11', predict_11, '12', predict_12, '13', predict_13, '14', predict_14,
                            '15', predict_15, '16', predict_16, '17', predict_17, '18', predict_18, '19', predict_19, 
                            '20', predict_20, '21', predict_21, '22', predict_22, '23', predict_23, '24', predict_24) as (rank_pre, value)""")).\
        where("""value is not null""")

    predict_upv.createOrReplaceTempView('predict_upv')
    actual_upv.createOrReplaceTempView('actual_upv')
    combined_df = spark.sql('''
    select f.msisdn, f.found_services, p.predicted_services from 
    (select msisdn, collect_set(array(rank_pre, value)) as predicted_services from predict_upv group by msisdn) p
     inner join 
     (select msisdn, collect_set(array(rank, value)) as found_services from actual_upv group by msisdn) f 
        on f.msisdn = p.msisdn''').persist(pyspark.StorageLevel.DISK_ONLY)
    combined_df.count()
    combined_df.show(5, truncate=False)
    return combined_df  # found_services, predicted_services

def convert_list_to_sorted_dict(_list):
    dic = dict()
    for arr in _list:
        dic[arr[1]] = int(arr[0])
    return dict(sorted(dic.items(), key=lambda item: item[1]))


def display_list(base):
    d_base = convert_list_to_sorted_dict(base)
    s = ''
    for k, v in d_base.items():
        s += k + '|'
    return s.split('|')[:-1]


def resolve_ranked(base, predict):
    d_base = convert_list_to_sorted_dict(base)
    d_predict = convert_list_to_sorted_dict(predict)
    s = ''
    for k, v in d_base.items():
        if k in d_predict:
            del d_predict[k]
        s += k + '|'
    for k, v in d_predict.items():
        s += k + '|'
    return s.split('|')[:-1]

resolve_ranked_udf = udf(lambda i, j: resolve_ranked(i, j), ArrayType(StringType()))
display_list_udf = udf(lambda i: display_list(i), ArrayType(StringType()))

if __name__ == "__main__":
    # LOAD CAC FILE MODEL DA LUU
    print("read file col train")
    Pkl_Filename = MULTICLASS_COLTRAIN_DIR + "_" + VERSION + ".pkl"
    #with open(Pkl_Filename, 'rb') as file: 
    #with open('/opt/spark/examples/col_train_202212.pkl', 'rb') as file:
    with client.read(Pkl_Filename) as f1:
        file = f1.read()
        selected_columns = pickle.loads(file)

    print(selected_columns)

    print("read file model")
    Pkl_Filename = MULTICLASS_MODEL_DIR + str(MULTI_ICONS) + "_" + VERSION + ".pkl"
    #with open(Pkl_Filename, 'rb') as file:
    #with open('/opt/spark/examples/Pipeline_Model_top6_202212.pkl', 'rb') as file:
    with client.read(Pkl_Filename) as f2:
        file2 = f2.read()
        loaded_model = pickle.loads(file2)

    print(loaded_model)

    print("read file encoder")
    encoder_name = MULTICLASS_ENCODER_DIR + str(MULTI_ICONS) + "_" + VERSION + ".pkl"
    #with open(encoder_name, 'rb') as f:
    #with open('/opt/spark/examples/label_encoder_top6_202212.pkl', 'rb') as f:
    with client.read(encoder_name) as f3:
        file3 = f3.read()
        le = pickle.loads(file3)

    print(le)     

    # LOAD FEATURE
    df_backtest_input = spark.read.parquet(MULTICLASS_FEAT_WRITE_DIR + '/%s' %VERSION + '/feature_icons').\
        select(selected_columns + ['msisdn']).persist(pyspark.StorageLevel.DISK_ONLY_2)
    df_backtest_input.count()

    # LOAD SERVICE TRANS TO EXPORT HOT SERVICES
    label_trans_input = spark.read.parquet(WRITE_LABEL_TRANS_DIR)

    print("begin split data and predict then merge")
    df_predict_full_input = multiclass_predict(df_backtest_input)
    print("mix predict result and hot services to full top 11 icons")
    df_final_input = mix_hot_services(label_trans_input, df_predict_full_input)

    print("convert pandas result to spark dataframe")
    res_input = spark.createDataFrame(df_final_input).persist(pyspark.StorageLevel.DISK_ONLY)
    res_input.count()
    print(res_input.show(2, False))
    #test
    predict_upv = res_input.select('msisdn',expr("""stack(24, '1', predict_1, '2', predict_2, '3', predict_3, '4', predict_4, '5', predict_5, '6', predict_6, '7', predict_7, '8', predict_8, '9', predict_9, '10', predict_10, '11', predict_11, '12', predict_12, '13', predict_13, '14', predict_14,'15', predict_15, '16', predict_16, '17', predict_17, '18', predict_18, '19', predict_19, '20', predict_20, '21', predict_21, '22', predict_22, '23', predict_23, '24', predict_24) as (rank_pre, value)""")).where("""value is not null""")
    print(predict_upv.show(3, False))
    combined_df_input = predict_upv_(res_input).persist(pyspark.StorageLevel.DISK_ONLY)
    combined_df_input.count()
    finalDf = combined_df_input.\
        withColumn('combined', resolve_ranked_udf(col('found_services'), col('predicted_services'))).\
        withColumn('f_services', display_list_udf(col('found_services'))).\
        withColumn('p_services', display_list_udf(col('predicted_services'))).\
        select('msisdn', 'f_services', 'p_services', 'combined').\
        cache()
    finalDf.count()
    finalDf.show(2, truncate=False)
    neededDf = finalDf.select(col('msisdn'),
                              col('combined').getItem(0).alias('predict_1'),
                              col('combined').getItem(1).alias('predict_2'),
                              col('combined').getItem(2).alias('predict_3'),
                              col('combined').getItem(3).alias('predict_4'),
                              col('combined').getItem(4).alias('predict_5'),
                              col('combined').getItem(5).alias('predict_6'),
                              col('combined').getItem(6).alias('predict_7'),
                              col('combined').getItem(7).alias('predict_8'),
                              col('combined').getItem(8).alias('predict_9'),
                              col('combined').getItem(9).alias('predict_10'),
                              col('combined').getItem(10).alias('predict_11'),
                              col('combined').getItem(11).alias('predict_12'),
                              col('combined').getItem(12).alias('predict_13'),
                              col('combined').getItem(13).alias('predict_14'),
                              col('combined').getItem(14).alias('predict_15'),
                              col('combined').getItem(15).alias('predict_16'),
                              col('combined').getItem(16).alias('predict_17'),
                              col('combined').getItem(17).alias('predict_18'),
                              col('combined').getItem(18).alias('predict_19'),
                              col('combined').getItem(19).alias('predict_20'),
                              col('combined').getItem(20).alias('predict_21'),
                              col('combined').getItem(21).alias('predict_22'),
                              col('combined').getItem(22).alias('predict_23'),
                              col('combined').getItem(23).alias('predict_24')).cache()
    neededDf.count()
    print('write')
    neededDf.coalesce(1).write.mode("overwrite").parquet(MULTICLASS_FINAL_RES_DIR + VERSION + '/predict_personal')
    
spark.stop()

